import logging
import os
import sys
from optparse import OptionParser

sys.path.append(os.getcwd())
from flask import jsonify, request

from project.controllers.room import RoomFeatureES
from project.controllers.calendar import CalendarES
from project.transform import transform_to_es

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO)
LOGGER = logging.getLogger(__name__)
config_path = "./config.yml"
from project.app import app, cache


@app.route("/")
def hello():
    return jsonify("Hello worlds")


@app.route("/v1/api/ls_recsys/similiar-rooms", methods=['GET'])
@cache.cached(timeout=15 * 60, query_string=True)
def get_category_by_shopid_and_product_id():
    '''
    room_id: int
    checkin: str(datetime format yyyy-mm-dd)
    checkout: str(datetime format yyyy-mm-dd)
    guest: int
    limit: int (default 4)
    :return:
    '''

    room_id = int(request.args.get("id"))
    checkin = request.args.get("checkin", "")
    checkout = request.args.get("checkout", "")
    number_of_guests = int(request.args.get("guest"))
    limit = int(request.args.get("limit", 4))
    version = int(request.args.get("v", 1))

    room_controller = RoomFeatureES()
    current_viewed_room = room_controller.get_by_id(room_id)
    if current_viewed_room is None:
        return jsonify({
            "id": "Not found room"
        })
    # LOGGER.info("query: %s", current_viewed_room)

    data = room_controller.load_model(current_viewed_room['id']) \
        .get_by_request(checkin, checkout, number_of_guests, room_id) \
        .get_similar_price(current_viewed_room['price'], threshold=30) \
        .get_nearby_location(current_viewed_room, threshold=5.0) \
        .get_similar_items(current_viewed_room) \
        .order_by("last_active", "desc") \
        .order_by("city", "desc") \
        .order_by("len_description", "desc") \
        .order_by("number_of_photos", "desc") \
        .fetch(limit)

    response = data
    return jsonify({
        "current": current_viewed_room,
        "suggest": response
    })


@app.route("/v1/api/ls_recsys/search", methods=['GET'])
@cache.cached(timeout=5 * 60, query_string=True)
def search_by_keyword():
    """
    checkin: str(datetime format yyyy-mm-dd)
    checkout: str(datetime format yyyy-mm-dd)
    :return:
    """
    user_input = request.args.get("user_input", None)
    limit = int(request.args.get("limit", 10))
    offset = (int(request.args.get("page", 1)) - 1) * limit
    version = int(request.args.get("v", 1))

    room_type_lst = request.args.getlist("room_type")
    property_type_lst = request.args.getlist('property_type')
    amenities_lst = request.args.getlist('amenities')
    guests = request.args.get("guests", default=0, type=int)
    beds = int(request.args.get("beds", default=1, type=int))
    bathrooms = int(request.args.get("bathrooms", default=1, type=int))
    bedrooms = int(request.args.get("bedrooms", default=1, type=int))
    min_price = int(request.args.get("min_price", default=10, type=int))
    max_price = int(request.args.get("max_price", default=750, type=int))
    map_details = request.args.get("map_details", None)
    location = request.args.get("location", "")
    order_by_price = request.args.get("order_by_price", "asc")
    checkin = request.args.get("checkin", None)
    checkout = request.args.get("checkout", None)

    room_controller = RoomFeatureES()
    requestObj = room_controller \
        .get_by_request(checkin, checkout, guests) \
        .filter_by_common(beds, bedrooms, bathrooms) \
        .filter_by_price(min_price, max_price)
    if user_input:
        requestObj = requestObj.filter_by_keyword(user_input)
    if len(amenities_lst) > 0:
        requestObj = requestObj.filter_by_amenities(amenities_lst)
    if len(property_type_lst) > 0:
        requestObj = requestObj.filter_by_property(property_type_lst)
    if len(room_type_lst) > 0:
        requestObj = requestObj.filter_by_room_type(room_type_lst)
    results, count = requestObj.order_by("price", order_by_price).fetch(limit, offset)

    return jsonify({
        "payload": results,
        "total_counts": count
    })


@app.route("/v1/api/ls_recsys/autocomplete", methods=['GET'])
@cache.cached(timeout=30 * 60, query_string=True)
def autocomplete():
    user_input = request.args.get("user_input")
    num_results = int(request.args.get("limit", 10))
    version = int(request.args.get("v", 1))

    room_controller = RoomFeatureES()
    results, count = room_controller.suggest_term(user_input, num_results)
    return jsonify({
        "payload": results,
        "total_counts": count
    })


@app.route("/v1/api/room/update/<int:room_id>", methods=['GET'])
def update_metadata(room_id):
    try:
        transform_to_es(app.config['MYSQL_CONN'], app.config['ES_CONN'], app.config['MYSQL_DBNAME'],
                        app.config['ES_DBNAME'], room_id=room_id)
        return jsonify({
            "success": True,
        })
    except Exception as e:
        LOGGER.error(e)
        return jsonify({
            "message": e,
            "success": False,
        })


@app.route("/v1/api/calendar/upsert", methods=['POST'])
def delete_calendar():
    try:
        payload = request.get_json()
        calendar = CalendarES()
        result = calendar.insert_update(payload)
        LOGGER.info("Upsert %s", result)
        return jsonify({
            "success": result
        })
    except Exception as e:
        LOGGER.error(e)
        return jsonify({
            "message": e,
            "success": False,
        })


@app.route("/v1/api/calendar/delete", methods=['POST'])
def upsert_calendar():
    try:
        payload = request.get_json()
        calendar = CalendarES()
        result = calendar.delete(payload)
        return jsonify({
            "success": result
        })
    except Exception as e:
        LOGGER.error(e)
        return jsonify({
            "message": e,
            "success": False,
        })


if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-p", "--port", dest="port", type=int,
                      help="Port")
    parser.add_option("-c", "--config", dest="config", type=str, default="config.yml",
                      help="Config")
    (options, args) = parser.parse_args()
    port = options.port
    # config_path = options.config
    # config = load_config(config_path)
    app.run('0.0.0.0', port=port, debug=False, threaded=False)
