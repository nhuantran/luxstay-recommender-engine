#Luxstay Recsys Engine

##Requirement
This project is required:
 - python3.6
 - pip>=9.0.2
 - mysql 5.6
 - elasticsearch >=2.1
 - redis

##Setup
- Install python3.6-pip
- Install all packages: 

    ```pip3.6 install -r requirements.txt```
    
- Change configuration in config.yml: 
    - mysql address, port,dbname; 
    - redis host,port
    - elasticsearch host,port
   
- Import database to ElasticSearch:

    ```python3.6 dataset_tools/es_create_cluster.py --config_path config.yml --model_path data/es_product_mapping.json --parser_path data/custom_analysis.json```
	```python3.6 batch_job/feature_transform.py config.yml```

- Install step:
    ```python3.6 run_server.py```

