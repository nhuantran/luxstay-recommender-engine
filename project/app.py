import logging
import os
import sys

sys.path.append(os.getcwd())

from flask import Flask
from flask_caching import Cache

from flask_sqlalchemy import SQLAlchemy

from utils.connection import _create_mysql_alchemy, create_es_connection, create_mysql_session
from utils.load_config import load_config

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO)
LOGGER = logging.getLogger(__name__)

# if len(sys.argv) > 1:
#     config = yaml.load(open(sys.argv[1]))
# else:
#     config = yaml.load(open("config_flask.yml"))
config = load_config()
es_conn = create_es_connection(config)
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = _create_mysql_alchemy(config)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
cache = Cache(config={'CACHE_TYPE': 'redis',
                      'CACHE_REDIS_HOST': config['REDIS']['HOST'],
                      'CACHE_REDIS_PORT': config['REDIS']['PORT']})
cache.init_app(app)
app.config['ES_CONN'] = es_conn
app.config['MYSQL_CONN'] = _create_mysql_alchemy(config)
app.config['MYSQL_DBNAME'] = config['MYSQL']['DBNAME']
app.config['ES_DBNAME'] = config['ES']['PRODUCT_INDEX']

if __name__ == "__main__":
    app.run('0.0.0.0', port=5000, debug=False, threaded=False)
