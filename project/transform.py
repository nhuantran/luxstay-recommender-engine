import os
import sys

sys.path.append(os.getcwd())
from datetime import datetime
import logging
import pandas as pd

# In[122]:
time_now = datetime.now()
DEFAULT_DATETIME = datetime(time_now.year, 1, 1)
FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO)
LOGGER = logging.getLogger(__name__)


def split_text(text, delimitter=" "):
    return len(text.split(delimitter))


def transform_to_es(mysql_conn, es_conn, mysql_dbname, es_conn_index="luxstay_room_feature", room_id=None):
    room_features_query = '''
    SELECT r.id,
    r_name_vi.value as room_name,
    r.accommodates,
    r.bedrooms as number_of_bedrooms,
    r.beds as number_of_beds, 
    r.bathrooms as room_bath,
    r.amenities as room_amenities,
    bt.name as bed_type, 
    bt.status as bed_status,
    pt.name as property_type,
    rt.description as room_type,
    r_desc_vi.value as room_description,
    rdes.space as room_space,
    rdes.access as device_access,
    room_photo.cnt as number_of_photos,
    rprice.night as price,
    rprice.guests as number_of_guests,
    calendar.last_active as last_active,
    state.name as state,
    city.name as city,
    country.name as country,
    raddress.latitude as lat,
    raddress.longitude as lon,
    r.property_type as room_prop_type_raw,
    r.room_type as room_type_raw

    FROM {dbname}.rooms r
    LEFT JOIN {dbname}.bed_type bt ON r.bed_type = bt.id 
    LEFT JOIN {dbname}.room_type as rt ON  rt.id = r.room_type 
    LEFT JOIN {dbname}.property_type as pt ON pt.id = r.property_type
    LEFT JOIN {dbname}.rooms_price as rprice ON rprice.room_id = r.id
    LEFT JOIN (SELECT rd.room_id, d.name from {dbname}.room_destination as rd, {dbname}.destinations as d WHERE rd.destination_id = d.id AND d.type = "state") as state ON r.id = state.room_id
    LEFT JOIN (SELECT rd.room_id, d.name FROM {dbname}.room_destination as rd, {dbname}.destinations as d WHERE rd.destination_id = d.id AND d.type = "city") as city ON r.id = city.room_id
    LEFT JOIN (SELECT rd.room_id, d.name FROM {dbname}.room_destination as rd, {dbname}.destinations as d WHERE rd.destination_id = d.id AND d.type = "country") as country on r.id = country.room_id
    LEFT JOIN (SELECT room_id, count(*) as cnt from {dbname}.rooms_photos group by room_id) as room_photo on r.id = room_photo.room_id
    LEFT JOIN (SELECT room_id, max(updated_at) as last_active FROM {dbname}.calendar group by room_id) as calendar on r.id = calendar.room_id
    LEFT JOIN rooms_description as rdes ON rdes.room_id = r.id
    LEFT JOIN rooms_address as raddress ON raddress.room_id = r.id
    LEFT JOIN rooms_locales as r_name_vi ON r_name_vi.room_id = r.id 
    LEFT JOIN rooms_locales as r_desc_vi ON r_desc_vi.room_id = r.id 
    WHERE r.room_type is not null
    AND r_name_vi.name = "name" AND r_name_vi.language_code = "vi"
    AND r_desc_vi.name = "summary" AND r_desc_vi.language_code = "vi"
    
    '''.format(dbname=mysql_dbname)
    if room_id is not None:
        room_features_query += " AND r.id = " + str(room_id)
    df = pd.read_sql_query(sql=room_features_query, con=mysql_conn, parse_dates=True, index_col=["id"])
    # df['room_type_raw'] = df['room_type_raw'].fillna(1).astype(int)
    df['len_description'] = df['room_description'].map(lambda d: split_text(d) if d is not None else 0)
    df['number_of_amenities'] = df['room_amenities'].map(lambda d: split_text(d, ",") if d is not None else 0)
    df['room_bath'] = df['room_bath'].fillna(1).astype(int)
    df['room_amenities_raw'] = df['room_amenities'].map(lambda d: [int(i) for i in d.split(",")] if d is not None else [])
    # df.to_sql(con=cnx, name="room_feature", if_exists="append")
    df.fillna({"last_active": DEFAULT_DATETIME}, inplace=True)
    items = df.reset_index().to_dict(orient='records')

    success_cnt = 0
    err_cnt = 0
    for item in items:
        try:
            item['address'] = "{state}, {city}, {country}".format(state=item['city'], city=item['state'],
                                                                  country=item['country'])
            item['lat'] = float(item['lat'])
            item['lon'] = float(item['lon'])
            item['location'] = {
                "lat": float(item['lat']),
                "lon": float(item['lon'])
            }
            es_conn.index(index=es_conn_index, doc_type="document", id=int(item['id']), body=item)
            success_cnt += 1
        except Exception as e:
            LOGGER.error("Error to parse %s: %s",item['id'],e)
            none_keys = []
            for key in item.keys():
                if item[key] == None:
                    none_keys.append(key)
            LOGGER.info("Empty fields: %s",",".join(none_keys))
            err_cnt +=1
    LOGGER.info("Found %s in queue", len(items))
    LOGGER.info("Done. Imported %s in ES",success_cnt)
    LOGGER.info("Error. Fail to imported %s in ES", err_cnt)
    return True
