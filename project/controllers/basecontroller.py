import os
import sys

sys.path.append(os.getcwd())
from utils.logger import LoggerFactory


class BaseController(object):
    def __init__(self, *args, **kwargs):
        self.logger = LoggerFactory.get_logger()
