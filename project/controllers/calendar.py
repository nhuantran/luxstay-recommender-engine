import sys

sys.path.append(".")

from utils.date_util import validate_datetime_by_fmt
from utils.connection import create_es_connection

from utils.load_config import load_config

NEXT_DAYS = 30


class CalendarES(object):
    def __init__(self, config=None):
        self.config = config or load_config('./config.yml')
        self.conn = create_es_connection(config)
        self.index = config['ES']['PRODUCT_INDEX']
        self.doc_type = "document"
        self.query_sort = []
        self.result = []
        self.callbacks = []
        self.date_format = '%Y-%m-%d'

    def insert_update(self, request: dict):
        payload = request["data"]
        room_id = payload["room_id"]
        is_valid = True
        if ~("start_date" in payload) or ~(validate_datetime_by_fmt(payload["start_date"], self.date_format)):
            is_valid = False
        if ~("end_date" in payload) or ~(validate_datetime_by_fmt(payload["end_date"], self.date_format)):
            is_valid = False
        start_date = payload["start_date"]
        end_date = payload["end_date"]
        status = payload["calendar_status"]
        price = payload.get("price", 0)
        notes = payload.get("notes", 0)
        room_locale = payload.get("room_locale", "vi")
        if is_valid:
            data = {
                "room_id": room_id,
                "start_date": start_date,
                "end_date": end_date,
                "status": status,
                "price": price,
                "notes": notes,
                "room_locale": room_locale
            }
            self.conn.index(index="calendar", doc_type="document", body=data)
        return is_valid

    def delete(self, request: dict):
        payload = request["data"]
        room_id = payload["room_id"]
        is_valid = True
        if ~("start_date" in payload) or ~(validate_datetime_by_fmt(payload["start_date"], self.date_format)):
            is_valid = False
        if ~("end_date" in payload) or ~(validate_datetime_by_fmt(payload["end_date"], self.date_format)):
            is_valid = False
        start_date = payload["start_date"]
        end_date = payload["end_date"]
        if is_valid:
            list_of_ids = self.conn.search(index="calendar", doc_type="document", body={
                "_source": "_id",
                "room_id": room_id,
                "start_date": start_date,
                "end_date": end_date
            })
            for doc_id in list_of_ids:
                self.conn.delete(index="calendar", id=doc_id, doc_type="document")
