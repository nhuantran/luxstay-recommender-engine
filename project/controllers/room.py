import sys
from copy import deepcopy
from datetime import datetime

sys.path.append(".")

from project.models import Calendar
from utils.data_util import make_onehot
from utils.connection import create_es_connection

from core.distance import hamming_distance
from utils.load_config import load_config

NEXT_DAYS = 30


class RoomFeatureES(object):
    def __init__(self, config=None):

        self.config = config or load_config('./config.yml')
        self.conn = create_es_connection(self.config)
        self.index = self.config['ES']['PRODUCT_INDEX']
        self.doc_type = "document"
        self.query_sort = []
        self.result = []
        self.callbacks = []
        self.date_format = '%Y-%m-%d'
        self.DEFAULT_FIELDS = [
            "accommodates",
            "address",
            "bed_type",
            "city",
            "country",
            "state"
            "lat",
            "lon",
            "price",
            "property_type",
            "room_amenities",
            "room_bath",
            "room_description",
            "room_name",
            "room_type",
        ]

    def load_model(self, room_id):
        return self

    def _search(self, query, fields=None, limit=1000):
        if fields:
            query['_source'] = fields
        items = self.conn.search(index=self.index, doc_type=self.doc_type, body=query, size=limit)
        results = []
        for item in items['hits']['hits']:
            tmp_item = deepcopy(item['_source'])
            tmp_item['room_id'] = int(item['_id'])
            results.append(tmp_item)
        count = items['hits']['total']
        pages = max(count / limit, 1.0)
        return results, count

    def _on_post_handle(self, results):
        # if self.query:
        #     self.query = {**query, **self.query}
        # else:
        #     self.query = query
        self.result = []
        for result in results:
            self.result.append(result['id'])
        return self

    def get_by_id(self, room_id):
        return self.conn.get(self.index, room_id)['_source']

    def get_by_request(self, checkin=None, checkout=None, number_of_guests=1, current_view_id=None):

        if checkin is None and checkout is None:
            # checkin_date = get_current_time(by="day")
            checkin_date = None
            checkout_date = None
            # checkout_date = get_current_time(by="day") + timedelta(days=NEXT_DAYS)
        else:
            checkin_date = datetime.strptime(checkin, self.date_format)
            checkout_date = datetime.strptime(checkout, self.date_format)

        room_ids = Calendar.check_not_available_rooms(checkin_date, checkout_date)
        query = {
            "filter": {
                "bool": {
                }
            }

        }
        if room_ids or len(room_ids) > 0:
            query['filter']['bool']['must_not'] = {
                "terms": {
                    "_id:": room_ids
                }
            }
        if number_of_guests == 0:
            query["filter"]["bool"] = {
                "must": {
                    "term": {
                        "number_of_guests": number_of_guests
                    }
                }
            }
        if current_view_id is not None:
            query["filter"]["bool"]["must_not"] = [
                {
                    "term": {
                        "_id": current_view_id
                    }
                }
            ]
        results, _ = self._search(query, fields=['id'])
        self._on_post_handle(results)
        return self

    def get_nearby_location(self, input_item, threshold=4.0):

        lat, lon = input_item['lat'], input_item['lon']
        if len(self.result) > 0:
            rooms_query = {
                "filter": {
                    "bool": {
                        "must": [
                            {
                                "terms": {
                                    "_id": self.result
                                },
                            },
                            {
                                "geo_distance": {
                                    "distance": "{threshold}km".format(threshold=threshold),
                                    "location": {
                                        "lat": lat,
                                        "lon": lon
                                    }

                                }
                            }
                        ]
                    }
                }
            }

            rooms, _ = self._search(rooms_query, fields=['lat', 'lon', 'id', 'city'])
            self._on_post_handle(rooms)

        return self

    def shuffle_room_type(self, result):
        return result

    def get_similar_price(self, price, threshold=20):
        price_low = price - threshold
        price_high = price + threshold
        query = {
            "filter": {
                "bool": {
                    "must": [
                        {
                            "range": {
                                "price": {
                                    "lte": price_high,
                                    "gte": price_low,
                                }
                            }
                        },
                        {"terms": {"_id": self.result}}
                    ]
                }
            }
        }
        results, _ = self._search(query, fields=['id'])
        self._on_post_handle(results)
        return self

    def get_similar_items(self, input_item):
        current_query = make_onehot(input_item['room_amenities'])
        if len(self.result) > 0:
            rooms_query = {
                "filter": {
                    "bool": {
                        "must": {"terms": {"_id": self.result}}
                    }
                }
            }

            rooms, _ = self._search(rooms_query)

            distances = []
            results = []
            for room in rooms:
                if room['room_amenities'] is None:
                    continue
                target = make_onehot(room['room_amenities'])
                distance = hamming_distance(current_query, target)
                distances.append((room, distance))

            distances = sorted(distances, key=lambda d: d[1])
            for room in distances:
                results.append(room[0])
            self._on_post_handle(results)
        return self

    def order_by(self, field_name, order_type="asc"):
        query = {field_name: {"order": order_type}}
        self.query_sort.append(query)
        return self

    # DONE
    def suggest_term(self, keyword, current_location=None, limit=10):
        distinct_address = set()
        distinct_rooms = set()
        #  Get address
        query = {
            "_source": ["address", "city", "state"],
            "query": {
                "bool": {
                    "should": [
                        {
                            "match": {
                                "address": {
                                    "query": keyword,
                                    "boost": 3,
                                    "minimum_should_match": "50%"
                                }
                            }
                        },

                    ]
                }
            }
        }
        result, count = self._search(query, limit=limit)
        for r in result:
            distinct_address.add(r['address'])
        query = {
            "_source": ["room_name"],
            "query": {
                "bool": {
                    "should": [
                        {
                            "match": {
                                "room_name": {
                                    "query": keyword,
                                    "boost": 3,
                                    "minimum_should_match": "50%"
                                }
                            }
                        },

                    ]
                }
            }
        }
        result, count = self._search(query, limit=limit)
        for r in result:
            distinct_rooms.add(r['room_name'])

        distinct_results = []

        for r in list(distinct_rooms):
            distinct_results.append({"text": r, "type": "room_name"})

        for r in list(distinct_address):
            distinct_results.append({"text": r, "type": "address"})

        return distinct_results, count

    def search_by_keyword(self, keyword, offset=0, limit=10):
        query = {
            "from": offset * limit, "size": limit,
            "query": {
                "match": {
                    "address": {
                        "query": keyword,
                        "operator": "and",
                        "type": "phrase"
                    }

                }
            },
            "highlight": {
                "fields": {
                    "address": {
                        "pre_tags": ["<span class='hit'>"],
                        "post_tags": ["</span>"]
                    }
                }
            }
        }
        query['sort'] = []
        for sort_item in self.query_sort:
            query['sort'].append(sort_item)
        result, count = self._search(query, limit=limit, offset=offset)
        if len(result) == 0:
            query = {
                "from": offset, "size": limit,
                "query": {
                    "match": {
                        "room_description": {
                            "query": keyword,
                            "operator": "and",
                            "minimum_should_match": "75%"
                        }

                    }
                }
            }
            result, count = self._search(query, limit=limit)

        return result, count

    def filter_by_amenities(self, amenities_lst):
        if len(self.result) > 0:
            rooms_query = {
                "filter": {
                    "bool": {
                        "must": [
                            {
                                "terms":
                                    {
                                        "_id": self.result
                                    }
                            }
                        ]
                    }
                }
            }
            for item in amenities_lst:
                rooms_query['filter']['bool']['must'].append({
                    "match": {
                        "room_amenities_raw": item
                    }
                })
            results, count = self._search(rooms_query)
            self._on_post_handle(results)
        return self

    def filter_by_property(self, properties):
        if len(self.result) > 0:
            rooms_query = {
                "filter": {
                    "bool": {
                        "must": [
                            {
                                "terms":
                                    {
                                        "_id": self.result
                                    }
                            }
                        ]
                    }
                }
            }
            for item in properties:
                rooms_query['filter']['bool']['must'].append({
                    "match": {
                        "room_prop_type_raw": int(item)
                    }
                })
            results, count = self._search(rooms_query)
            self._on_post_handle(results)
        return self

    def filter_by_room_type(self, room_types):
        if len(self.result) > 0:
            rooms_query = {
                "filter": {
                    "bool": {
                        "must": [
                            {
                                "terms":
                                    {
                                        "_id": self.result
                                    }
                            }
                        ]
                    }
                }
            }
            for item in room_types:
                rooms_query['filter']['bool']['must'].append({
                    "match": {
                        "room_type_raw": int(item)
                    }
                })
            results, count = self._search(rooms_query)
            self._on_post_handle(results)
        return self

    def filter_by_price(self, min_price, max_price):
        if len(self.result) > 0:
            rooms_query = {

                "filter": {
                    "bool": {
                        "must": [
                            {
                                "terms":
                                    {
                                        "_id": self.result
                                    }
                            },
                            {
                                "range": {
                                    "price": {
                                        "gte": min_price,
                                        "lte": max_price
                                    }
                                }
                            }
                        ]
                    }
                }
            }
            results, count = self._search(rooms_query)
            self._on_post_handle(results)
        return self

    def filter_by_common(self, number_of_beds, number_of_bedrooms, number_of_bathrooms):
        if len(self.result) > 0:
            rooms_query = {
                "filter": {
                    "bool": {
                        "must": [
                            {
                                "terms": {
                                    "_id": self.result
                                }
                            },
                            {
                                "term": {
                                    "number_of_bedrooms": number_of_bedrooms,
                                }
                            },
                            {
                                "term": {
                                    "number_of_beds": number_of_beds
                                }
                            },
                            {
                                "term": {
                                    "room_bath": number_of_bathrooms
                                }
                            }
                        ]
                    }
                },
            }
            results, count = self._search(rooms_query)
            self._on_post_handle(results)
        return self

    def filter_by_keyword(self, user_input):
        if len(self.result) > 0:
            rooms_query = {
                "query": {
                    "bool": {
                        "should": [
                            {
                                "match": {
                                    "address": {
                                        "query": user_input,
                                        "boost": 3,
                                        "minimum_should_match": "50%"
                                    }
                                }
                            },
                            {
                                "match": {
                                    "room_name": {
                                        "query": user_input,
                                        "boost": 1,
                                        "operator": "and",
                                        "minimum_should_match": "65%"

                                    }
                                }
                            }
                        ]
                    }
                }
            }
            results, count = self._search(rooms_query)
            self._on_post_handle(results)
        return self

    def fetch(self, limit=1000, offset=0):
        rooms_query = {
            "from": offset * limit, "size": limit,
            "filter": {
                "bool": {
                    "must": {"terms": {"_id": self.result}}
                }
            }
        }
        rooms_query['sort'] = []
        for sort_item in self.query_sort:
            rooms_query['sort'].append(sort_item)
        results, count = self._search(rooms_query, limit=limit, fields=self.DEFAULT_FIELDS)
        return results, count
