import logging
import sys

sys.path.append(".")
from datetime import datetime
# External Imports
from sqlalchemy import DateTime, BigInteger, Float, VARCHAR, INTEGER, TEXT, FLOAT, Enum, TIMESTAMP, SMALLINT, DECIMAL, \
    or_
from sqlalchemy import create_engine, func, desc, asc
from sqlalchemy.ext.declarative import declarative_base

# Custom Imports
from project.app import db
from core.distance import hamming_distance, haversine_distance
from core.logger import LoggingMixin
from utils.load_config import load_config
from utils.data_util import make_onehot

DeclarativeBase = declarative_base()


class RoomFeature(db.Model):
    __tablename__ = 'room_feature'
    date_format = '%Y-%m-%d'
    id = db.Column(BigInteger, autoincrement=True, primary_key=True)
    room_name = db.Column('room_name', VARCHAR(256, collation='utf8_unicode_ci'), nullable=True)
    accommodates = db.Column("accommodates", Float, nullable=True)
    number_of_bedrooms = db.Column("number_of_bedrooms", INTEGER, nullable=True)
    number_of_beds = db.Column("number_of_beds", INTEGER, nullable=True)
    room_bath = db.Column("room_bath", INTEGER, nullable=True)
    room_amenities = db.Column("room_amenities", VARCHAR(512), nullable=True)
    bed_type = db.Column("bed_type", VARCHAR(128, collation='utf8_unicode_ci'), nullable=True)
    bed_status = db.Column('bed_status', VARCHAR(128, collation='utf8_unicode_ci'), nullable=True)
    property_type = db.Column("property_type", VARCHAR(128, collation='utf8_unicode_ci'))
    room_type = db.Column("room_type", VARCHAR(128, collation='utf8_unicode_ci'))
    room_description = db.Column("room_description", TEXT(collation='utf8_unicode_ci'), nullable=True)
    room_space = db.Column("room_space", TEXT(collation='utf8_unicode_ci'), nullable=True)
    device_access = db.Column('device_access', TEXT(collation='utf8_unicode_ci'), nullable=True)
    number_of_photos = db.Column('number_of_photos', INTEGER, default=0)
    len_description = db.Column('len_description', INTEGER, default=0)
    number_of_amenities = db.Column('number_of_amenities', INTEGER)
    price = db.Column('price', INTEGER)
    number_of_guests = db.Column('number_of_guests', INTEGER)
    last_active = db.Column('last_active', DateTime, nullable=True)
    state = db.Column('state', VARCHAR(128, collation='utf8_unicode_ci'), nullable=True)
    city = db.Column('city', VARCHAR(128, collation='utf8_unicode_ci'), nullable=True)
    country = db.Column('country', VARCHAR(128, collation='utf8_unicode_ci'), nullable=True)
    lat = db.Column('lat', Float, nullable=True)
    lon = db.Column('lon', Float, nullable=True)
    room_amenities_lst = []
    room_accomondation = []

    __table_args__ = (
        db.Index('ti_price', price),
    )

    def __init__(self, *args, **kwargs):
        super(RoomFeature, self).__init__(*args, **kwargs)
        self.distance = 0
        self.result = []
        self._fetch = RoomFeature.query
        self._ranking = []

    def to_json(self):
        return {"id": self.id,
                "name": self.room_name,
                "last_active": self.last_active,
                "price": self.price,
                "number_of_photos": self.number_of_photos,
                "state": self.state,
                "city": self.city}

    def get_by_id(self, room_id):
        return RoomFeature.query.filter_by(id=room_id).first()

    def load_model(self, room_id):
        return self

    def get_by_request(self, checkin, checkout, number_of_guests, current_view_id=None):
        checkin_date = datetime.strptime(checkin, self.date_format)
        checkout_date = datetime.strptime(checkout, self.date_format)
        room_ids = Booking.check_available_rooms(checkin_date, checkout_date)
        query = RoomFeature.query.filter(RoomFeature.number_of_guests == number_of_guests) \
            .filter(~RoomFeature.id.in_(room_ids))
        if current_view_id is not None:
            query = query.filter(RoomFeature.id != current_view_id)
        rooms = query.all()
        self._on_post_handle(rooms)
        return self

    def get_all(self):
        return RoomFeature.query.all()

    def _on_post_handle(self, items):
        self.result = []
        for item in items:
            self.result.append(item.id)

    def get_similar_price(self, price, threshold=20):
        rooms_query = RoomFeature.query.filter(func.abs(RoomFeature.price - price) <= threshold)
        if len(self.result) > 0:
            rooms_query = rooms_query.filter(RoomFeature.id.in_(self.result))
        rooms = rooms_query.all()
        self._on_post_handle(rooms)
        return self

    def get_nearby_location(self, input_item, threshold=4.0):
        rooms_query = RoomFeature.query
        lat, lon = input_item.lat, input_item.lon
        if len(self.result) > 0:
            rooms_query = rooms_query.filter(RoomFeature.id.in_(self.result))

        rooms = rooms_query.all()
        distances = []
        results = []
        for room in rooms:
            if room.lat is None:
                continue
            distance = haversine_distance((room.lat, room.lon), (lat, lon)) / 1000.0
            if distance <= threshold:
                distances.append((room, distance))

        distances = sorted(distances, key=lambda d: d[1])
        for room in distances:
            results.append(room[0])
        self.distances = distances
        self._on_post_handle(results)
        return self

    def get_similar_items(self, input_item):
        current_query = make_onehot(input_item.room_amenities)
        rooms_query = RoomFeature.query
        if len(self.result) > 0:
            rooms_query = rooms_query.filter(RoomFeature.id.in_(self.result))

        rooms = rooms_query.all()
        distances = []
        results = []
        for room in rooms:
            if room.room_amenities is None:
                continue
            target = make_onehot(room.room_amenities)
            distance = hamming_distance(current_query, target)
            distances.append((room, distance))

        distances = sorted(distances, key=lambda d: d[1])
        for room in distances:
            results.append(room[0])
        self._on_post_handle(results)
        return self

    def order_by(self, field_name, order_type="asc"):
        func_order = desc if order_type == "desc" else asc
        # self._fetch = self._fetch.order_by(func_order(field_name))
        self._ranking.append(func_order(field_name))
        return self

    def __repr__(self):
        return "id={id},state={state}, city={city}, lat={lat}, lon={lon},price={price},last_active={last_active}".format(
            id=self.id,
            state=self.state,
            city=self.city,
            price=self.price,
            lat=self.lat,
            lon=self.lon,
            last_active=self.last_active)

    def fetch(self, limit=4):
        self.result = self.result[:limit]
        queries = self._fetch.filter(RoomFeature.id.in_(self.result))
        if len(self._ranking) > 0:
            for rank in self._ranking:
                queries = queries.order_by(rank)
        # print(queries)
        data = queries.all()
        for idx, room in enumerate(data):
            room.distance = self.distances[idx]
        return data


class RoomSearch(LoggingMixin):
    """
    Model searching, support chaining filter in memory
    """
    # Array of RoomFeature
    listings = []

    def filter_by_price(self):
        return self


class Calendar(db.Model):
    __tablename__ = 'calendar'
    id = db.Column('id', BigInteger, primary_key=True, nullable=False)
    room_id = db.Column('room_id', INTEGER, nullable=False)
    date = db.Column('date', DateTime(), nullable=False)
    status = db.Column('status', VARCHAR(collation='utf8_unicode_ci', length=255))
    price = db.Column('price', INTEGER)

    @staticmethod
    def check_not_available_rooms(checkin_date: datetime, checkout_date: datetime):
        query = Calendar.query
        if checkin_date and checkout_date:
            query = query.filter(Calendar.date >= checkin_date) \
                    .filter(Calendar.date <= checkout_date)
        query = query.filter(Calendar.status != 'Available').distinct(Calendar.room_id)
        data = query.all()
        result = [item.room_id for item in data]
        return result


class Booking(db.Model):
    __tablename__ = 'bookings'
    booking_id = db.Column('id', INTEGER, primary_key=True, nullable=False)
    code = db.Column('code', VARCHAR(collation='utf8_unicode_ci', length=20), nullable=False)
    auth_code = db.Column('auth_code', VARCHAR(collation='utf8_unicode_ci', length=20), nullable=False)
    room_id = db.Column('room_id', INTEGER(), nullable=False)
    host_id = db.Column('host_id', INTEGER(), nullable=False)
    user_id = db.Column('user_id', INTEGER())
    checkin = db.Column('checkin', DateTime(), nullable=False)
    checkout = db.Column('checkout', DateTime(), nullable=False)
    number_of_guests = db.Column('number_of_guests', INTEGER(), nullable=False)
    nights = db.Column('nights', INTEGER(), nullable=False)
    per_night = db.Column('per_night', FLOAT(), nullable=False)
    subtotal = db.Column('subtotal', INTEGER(), nullable=False)
    cleaning = db.Column('cleaning', INTEGER(), nullable=False)
    additional_guest = db.Column('additional_guest', INTEGER(), nullable=False)
    security = db.Column('security', INTEGER(), nullable=False)
    service = db.Column('service', FLOAT(), nullable=False)
    host_fee = db.Column('host_fee', INTEGER(), nullable=False)
    total = db.Column('total', FLOAT(), nullable=False)
    coupon_code = db.Column('coupon_code', VARCHAR(collation='utf8_unicode_ci', length=50), nullable=False)
    lst_amount = db.Column('lst_amount', INTEGER(), nullable=False)
    coupon_amount = db.Column('coupon_amount', INTEGER(), nullable=False)
    currency_code = db.Column('currency_code', VARCHAR(collation='utf8_unicode_ci', length=10), nullable=False)
    paymode = db.Column('paymode', VARCHAR(collation='utf8_unicode_ci', length=50))
    cancellation = db.Column('cancellation', Enum('Flexible', 'Moderate', 'Strict', collation='utf8_unicode_ci'),
                             nullable=False)
    status = db.Column('status', VARCHAR(collation='utf8_unicode_ci', length=25))
    type = db.Column('type', Enum('contact', 'reservation', collation='utf8_unicode_ci'))
    cancelled_by = db.Column('cancelled_by', Enum('Account', 'Guest', 'Host', 'Account', collation='utf8_unicode_ci'))
    cancelled_reason = db.Column('cancelled_reason', VARCHAR(collation='utf8_unicode_ci', length=500), nullable=False)
    decline_reason = db.Column('decline_reason', TEXT(collation='utf8_unicode_ci'), nullable=False)
    note = db.Column('note', TEXT(collation='utf8_unicode_ci'))
    accepted_at = db.Column('accepted_at', TIMESTAMP(), nullable=False)
    expired_at = db.Column('expired_at', TIMESTAMP(), nullable=False)
    declined_at = db.Column('declined_at', TIMESTAMP(), nullable=False)
    cancelled_at = db.Column('cancelled_at', TIMESTAMP(), nullable=False)
    created_at = db.Column('created_at', TIMESTAMP(), nullable=False)
    updated_at = db.Column('updated_at', TIMESTAMP(), nullable=False)
    crawl = db.Column('crawl', SMALLINT())
    invoiced_invoice = db.Column('invoiced_invoice', VARCHAR(collation='utf8_unicode_ci', length=20), nullable=False)
    locaiton = db.Column('location', VARCHAR(collation='utf8_unicode_ci', length=25))
    payment_status = db.Column('payment_status', VARCHAR(collation='utf8_unicode_ci', length=25))
    bank_prefix = db.Column('bank_prefix', VARCHAR(collation='utf8_unicode_ci', length=10))
    crawl_at = db.Column('crawl_at', TIMESTAMP())
    checkout_at = db.Column('checkout_at', TIMESTAMP())
    channel = db.Column('channel', VARCHAR(collation='utf8_unicode_ci', length=255))
    crawl_report = db.Column('crawl_report', SMALLINT(), nullable=False)
    payout = db.Column('payout', FLOAT(asdecimal=True), nullable=False)
    crawl_payout = db.Column('crawl_payout', SMALLINT(), nullable=False)
    sms_remind = db.Column('sms_remind', SMALLINT(), nullable=False)
    refund = db.Column('refund', DECIMAL(precision=15, scale=2), nullable=False)
    notify_at = db.Column('notify_at', TIMESTAMP())
    invite_amount = db.Column('invite_amount', FLOAT(precision=18, scale=2, asdecimal=True), nullable=False)
    used_credit = db.Column('used_credit', FLOAT(precision=18, scale=2, asdecimal=True), nullable=False)

    @staticmethod
    def check_available_rooms(checkin_date: datetime, checkout_date: datetime):
        month = min(checkin_date.month, checkout_date.month)
        year = min(checkout_date.year, checkin_date.year)
        query = Booking.query \
            .filter(func.year(Booking.checkin) >= year) \
            .filter(func.month(Booking.checkin) >= month) \
            .filter(or_(Booking.checkin < checkin_date, Booking.checkout > checkin_date)) \
            .filter(or_(Booking.checkin < checkin_date, Booking.checkout > checkin_date)) \
            .filter(or_(Booking.checkin < checkin_date, Booking.checkout > checkout_date)) \
            .distinct(Booking.room_id)
        query = Booking.query
        data = query.all()
        result = [item.room_id for item in data]
        return result


class SearchRequest(object):
    user_input = "hanoi"
    limit = 10
    page = 1
    type = "search"
    checkin = "2018-01-01"
    checkout = "2018-01-31"
    stype = ""
    room_type = []
    property_type = []
    amenities = []
    guests = 0
    beds = 1
    bathrooms = 1
    bedrooms = 1
    min_price = 10
    max_price = 750
    order_by_price = True
    instance_book = False
    location = "hanoi"
    sid = ""


def syncdb(cfg=None):
    logging.info('Initialising the database.')
    host, port, username, password, dbname = cfg['MYSQL']['HOST'], cfg['MYSQL']['PORT'], \
                                             cfg['MYSQL']['USERNAME'], cfg['MYSQL']['PASSWORD'], \
                                             cfg['MYSQL']['DBNAME']

    engine_new = create_engine('mysql+mysqlconnector://{username}:{password}@{host}:{port}/{dbname}?charset=utf8'
                               .format(username=username,
                                       password=password,
                                       host=host,
                                       port=port,
                                       dbname=dbname))
    # Syncdb
    DeclarativeBase.metadata.create_all(engine_new)


if __name__ == '__main__':
    config = load_config(sys.argv[1])
    syncdb(config)
