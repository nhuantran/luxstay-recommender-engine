from utils.logger import bk_logger


class LoggingMixin(object):
    @property
    def bk_logger(self):
        return bk_logger
