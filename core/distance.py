import numpy as np


def building_features(text_vec, number_of_amenties):
    vector = [0] * (number_of_amenties)
    if text_vec is not None:
        for i in text_vec.split(","):
            vector[int(i) - 1] = 1
    return np.array(vector)


def hamming_distance(s1, s2):
    count = 0
    for i in range(len(s1)):
        if s1[i] == s2[i]:
            count += 1
    return count * 1.0 / len(s1)


def haversine_distance(s1, s2):
    if isinstance(s1, tuple) and isinstance(s2, tuple):
        R = 6371e3
        delta = np.deg2rad(s2[0] - s1[0])
        gamma = np.deg2rad(s2[1] - s1[1])
        rad_lat1 = np.deg2rad(s1[0])
        rad_lat2 = np.deg2rad(s2[0])

        a = np.square(np.sin(delta / 2)) + np.cos(rad_lat1) * np.cos(rad_lat2) * np.square(np.sin(gamma / 2))
        c = 2 * np.arctan2(np.sqrt(a), np.sqrt(1 - a))
        d = R * c
        return d
def sigmoid(x):
    return 1.0 / (np.exp(x-5)+1)

def exp(x):
    return np.exp(-x)