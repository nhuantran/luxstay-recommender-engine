import os
import sys

sys.path.append(os.getcwd())
from datetime import datetime
from project.transform import transform_to_es, split_text
import pandas as pd
import yaml
from utils.connection import _create_mysql_alchemy, create_es_connection
from utils.load_config import load_config

# In[122]:
time_now = datetime.now()
DEFAULT_DATETIME = datetime(time_now.year, 1, 1)



def main(config_path="./config.yml"):
    config = load_config(config_path)
    dbname = config['MYSQL']['DBNAME']
    mysql_conn = _create_mysql_alchemy("config.yml")
    es_conn = create_es_connection(config)
    index = config['ES']['PRODUCT_INDEX']

    transform_to_es(mysql_conn,es_conn,dbname,index)

if __name__ == '__main__':
    config_path = sys.argv[1]
    main(config_path)
