import logging
from logging.handlers import TimedRotatingFileHandler

from pythonjsonlogger import jsonlogger

from utils.date_util import *


class TimeLogger(object):
    def __init__(self):
        FORMAT = '%(asctime)-15s %(message)s'
        logging.basicConfig(filename="logs/log_dispatcher/log_dispatcher.log", format=FORMAT, level=logging.INFO)
        LOGGER = logging.getLogger(__name__)
        self.LOGGER = LOGGER

    def info(self, message, *args):
        self.LOGGER.info(message, *args)

    def debug(self, message, *args):
        self.LOGGER.debug(message, *args)

    def warn(self, message, *args):
        self.LOGGER.warning(message, *args)

    def error(self, message, *args):
        self.LOGGER.exception(message, *args)

    def exception(self, message, *args):
        self.LOGGER.exception(message, *args)


class FileLogger(TimedRotatingFileHandler):
    def __init__(self, *args, **kwargs):
        super(FileLogger, self).__init__(*args, **kwargs)

        format_str = '%(levelname)%(asctime)%(message)'
        formatter = CustomJsonFormatter(format_str)

        LOGGER = logging.getLogger(__name__)
        self.LOGGER = LOGGER
        self.setFormatter(formatter)
        self.LOGGER.addHandler(self)

    def info(self, message, *args):
        self.LOGGER.info(message, *args)

    def debug(self, message, *args):
        self.LOGGER.debug(message, *args)

    def warn(self, message, *args):
        self.LOGGER.warning(message, *args)

    def error(self, message, *args):
        self.LOGGER.exception(message, *args)

    def exception(self, message, *args):
        self.LOGGER.exception(message, *args)


class LoggerFactory(object):
    @staticmethod
    def get_logger(name="time"):
        logger = FileLogger(filename="logs/log_dispatcher/log_dispatcher.log", delay=True, when='D', encoding='utf-8')
        logger.setLevel(logging.ERROR)
        # logger = TimeLogger()
        return logger


class CustomJsonFormatter(jsonlogger.JsonFormatter):
    def add_fields(self, log_record, record, message_dict):
        super(CustomJsonFormatter, self).add_fields(log_record, record, message_dict)
        if not log_record.get('timestamp'):
            # this doesn't use record.created, so it is slightly off
            now = get_datetime_str()
            log_record['ts'] = now
            del log_record["asctime"]

        if log_record.get('level'):
            log_record['level'] = log_record['level'].lower()
            del log_record["levelname"]
        else:
            log_record['level'] = record.levelname.lower()
            del log_record["levelname"]

        if log_record.get('message'):
            log_record['msg'] = log_record['message']
            del log_record["message"]
        else:
            log_record['msg'] = record.message
            del log_record["message"]


bk_logger = LoggerFactory.get_logger()
