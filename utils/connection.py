import logging
import os
import sys

sys.path.append(os.getcwd())
from elasticsearch import Elasticsearch
from redis import StrictRedis
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy import create_engine, MetaData
from utils.load_config import load_config

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO)
LOGGER = logging.getLogger(__name__)


def create_es_connection(config):
    clusters = config['ES']['HOSTS']
    clusters_lst = []
    for node in clusters:
        ip, port = node.split(":")
        clusters_lst.append({"host": ip, "port": port})

    es = Elasticsearch(clusters_lst,
                       use_ssl=config['ES']['SSL']['enable'],
                       verify_certs=config['ES']['SSL']['verify_certs'],
                       ca_certs=config['ES']['SSL']['ca_certs'],
                       client_cert=config['ES']['SSL']['client_cert'],
                       client_key=config['ES']['SSL']['client_key'])
    return es


def create_redis_connection(config_path="./config.yml"):
    config = load_config(config_path)
    REDIS_HOST = config['REDIS']['HOST']
    REDIS_PORT = config['REDIS']['PORT']
    client_redis = StrictRedis(host=REDIS_HOST, port=REDIS_PORT)
    return client_redis


def _create_mysql_alchemy(config_path):
    if isinstance(config_path, str):
        config = load_config(config_path)
    else:
        config = config_path
    MYSQL_HOST = config['MYSQL']['HOST']
    MYSQL_PORT = config['MYSQL']['PORT']
    MYSQL_USERNAME = config['MYSQL']['USERNAME']
    MYSQL_PASSWORD = config['MYSQL']['PASSWORD']
    MYSQL_DBNAME = config['MYSQL']['DBNAME']
    MYSQL_RECONNECT = config['MYSQL']['RECYCLE']
    connection_link = "mysql+mysqlconnector://{user}:{password}@{host}:{port}/{schema}".format(user=MYSQL_USERNAME,
                                                                                               password=MYSQL_PASSWORD,
                                                                                               host=MYSQL_HOST,
                                                                                               port=MYSQL_PORT,
                                                                                               schema=MYSQL_DBNAME)
    return connection_link


def create_mysql_session(config_path="./config.yml"):
    config = load_config(config_path)
    engine = create_engine(_create_mysql_alchemy(config), pool_pre_ping=True, pool_size=5)
    Session = scoped_session(
        sessionmaker(autocommit=False, autoflush=False, bind=engine))
    session = Session()
    return session


def generate_table_model(table_name):
    session = _create_mysql_alchemy(load_config())
    metadata = MetaData(session)
    metadata.reflect()
    return metadata.tables.get(table_name)
