from hashlib import sha256

import yaml


def get_config(config_path='./config.yml'):
    with open(config_path) as ymlfile:
        cfg = yaml.load(ymlfile)

    return cfg


def default(obj):
    """Default JSON serializer."""
    import datetime

    if isinstance(obj, datetime.datetime):
        return int(obj.strftime("%s"))

    raise TypeError('Not sure how to serialize %s' % (obj,))


def hash_text(text):
    return sha256(text.encode('utf-8')).hexdigest()


# def aliexpress_hash_image(image_url):
#     return hash_text(image_url.replace("50x50", "640x640"))
#
#
# # Return image in memory
# def download_image(image_src, dest=None):
#     content = requests.get(image_src)
#     img = Image.frombytes(content)
#     with open(dest, "wb") as f:
#         f.write(content)
#     return img


def resize_and_clean_url(url):
    if "shopify" in url:
        img_src = url.split("?")[0]
        img_src_split = img_src.split(".")
        img_src_split[-2] = img_src_split[-2] + "_640x640"
        img_src_resized = ".".join(img_src_split)
        return img_src_resized
    return url


def get_image_id(url):
    image_url = url.strip().replace("50x50", "640x640")
    image_id = sha256(image_url.encode('utf-8')).hexdigest()
    return image_id


def make_onehot(amenities, number_of_features=64):
    vector = [0] * number_of_features
    for amenity_id in amenities.split(","):
        vector[int(amenity_id)] = 1
    return vector
