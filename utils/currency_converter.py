import json

from core import HttpClient
from utils.connection import create_redis_connection


def convert_to_usd(amount=None, currency_unit=None):
    draft_currency_unit_array = ['HKD', 'AUD', 'EUR', 'GBP']
    draft_currency_unit = 'SGD'

    if not amount or not currency_unit or currency_unit == 'USD':
        money_amount_in_usd = amount
    else:
        # convert to usd
        money_amount_in_usd = convert_currency(amount=amount, from_unit=currency_unit, to_unit='USD')

        if not money_amount_in_usd:
            # Convert to SGD
            draft_converted = convert_currency(amount=amount, from_unit=currency_unit, to_unit=draft_currency_unit)

            # If cannot convert to SGD
            if not draft_converted:
                for currency in draft_currency_unit_array:
                    draft_currency_unit = currency
                    draft_converted = convert_currency(amount=amount, from_unit=currency_unit, to_unit=draft_currency_unit)
                    if draft_converted:
                        break

            # Convert to USD
            money_amount_in_usd = convert_currency(amount=draft_converted, from_unit=draft_currency_unit, to_unit='USD')

    return float(money_amount_in_usd)


def convert_currency(amount=None, from_unit=None, to_unit=None):
    app_id = 'f869f0c6f13c4e5d8fb9f7a3f680890d'

    from_unit = correct_currency_format(from_unit)
    to_unit = correct_currency_format(to_unit)

    if from_unit == to_unit:
        result = amount
    else:
        result = amount
        client_redis = create_redis_connection()

        cache_key = 'oer_currency_convert_' + from_unit + "_" + to_unit

        if client_redis.exists(cache_key):
            converted_unit = client_redis.fetch(cache_key)
            result = float(converted_unit) * amount

        else:
            # Open exchange rate API finance
            # Get base rate from usd
            url = 'http://openexchangerates.org/api/latest.json?app_id=' + app_id

            # Get file from Google API
            http_client = HttpClient()
            data = http_client.get_raw(url=url)

            if data is None:
                return round(result, 2)

            data = json.loads(data)

            if "rates" not in data:
                return round(result, 2)

            # Get value converted
            rates = data['rates']

            if from_unit not in rates:
                return round(result, 2)

            usd_from_rate = rates[from_unit]
            from_to_usd = 1 / usd_from_rate

            if to_unit not in rates:
                return round(result, 2)

            usd_to_rate = rates[to_unit]
            converted_unit = from_to_usd * usd_to_rate
            converted_unit = round(converted_unit, 7)

            # Cache currency convert
            client_redis.save(cache_key, str(converted_unit), 86400*2)

            result = converted_unit * amount

    # Format 2 decimal
    result = round(result, 2)

    return result


def correct_currency_format(currency=None):
    incorrect_currencies = get_incorrect_currency_format()
    if currency in incorrect_currencies:
        return incorrect_currencies[currency]

    return currency


def get_incorrect_currency_format():
    return {
        'DT': 'TND'
    }
