from hashlib import sha256

import yaml


def load_config(file_path="../../config_cluster.yml"):
    return yaml.load(open(file_path))


def hash_text(text):
    return sha256(text).hexdigest()


def resize_and_clean_url(url):
    img_src = url.split("?")[0]
    img_src_split = img_src.split(".")
    img_src_split[-2] = img_src_split[-2] + "_640x640"
    img_src_resized = ".".join(img_src_split)
    return img_src_resized
