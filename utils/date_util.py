import json
import time
from datetime import datetime

import pendulum

utc = pendulum.timezone('UTC')

DATETIME_FORMAT = "%Y-%m-%d_%H:%M:%S"


def get_datetime_str():
    return datetime.today().strftime(DATETIME_FORMAT)


def get_datetime_str_by_custom_format(format_time):
    return datetime.today().strftime(format_time)


def get_datetime():
    return datetime.today()


def convert_date_time_to_format(s):
    return s.strptime(DATETIME_FORMAT)


def convert_time_stamp_to_format(s):
    return datetime.fromtimestamp(int(s)).strftime(DATETIME_FORMAT)


def convert_time_stamp_to_custom_format(s, format_time):
    return datetime.fromtimestamp(int(s)).strftime(format_time)


def days_from_now(previous_day=None):
    if isinstance(previous_day, int):
        previous_day = datetime.utcfromtimestamp(previous_day)
    previous_day = previous_day or datetime.now()
    now = datetime.now()
    days = max((now - previous_day).days, 0)
    return days


def convert_datetime_to_json(o):
    if isinstance(o, datetime):
        try:
            return datetime.strftime(o, DATETIME_FORMAT)
        except Exception as e:
            print(e)
    return o


class DateTimeEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            # return o.strftime(DATETIME_FORMAT)
            return convert_datetimeobject_to_json(o)
        return json.JSONEncoder.default(self, o)


def convert_datetime_to_timestamp(s):
    return int(time.mktime(datetime.strptime(s, DATETIME_FORMAT).timetuple()))


def convert_date_to_timestamp(s):
    return int(time.mktime(s.timetuple()))


def convert_datetimeobject_to_json(date_time):
    # sample: "affiliate_ps_updated": {"date": "2018-01-06 07:57:12.000000", "timezone_type": 3, "timezone": "UTC"}
    date_time = date_time or datetime.now()
    timezone_type = 3
    timezone = time.tzname[0]
    time_str = str(date_time)
    return {
        "date": time_str,
        "timezone_type": timezone_type,
        "timezone": timezone
    }


def get_utcnow():
    d = datetime.utcnow()
    d = d.replace(tzinfo=utc)
    return d


def days_past(current_date, compare_with=None):
    if isinstance(current_date, int):
        current_date = datetime.utcfromtimestamp(current_date)
    if compare_with is None:
        compare_with = datetime.now()
    if isinstance(compare_with, int):
        compare_with = datetime.utcfromtimestamp(compare_with)

    past = current_date - compare_with
    return past.days


def format_datetime_from_unix(unix_time, fmt):
    dt = datetime.utcfromtimestamp(unix_time)
    return dt.strftime(fmt)


def get_current_time(by="minute",next_month=True):
    """

    :param by: minute, hour, day
    :return: datetime
    """
    time_now = datetime.now()
    if by == "minute":
        return datetime(time_now.year, time_now.month, day=time_now.day, hour=time_now.hour, minute=time_now.minute)
    elif by == "hour":
        return datetime(time_now.year, time_now.month, day=time_now.day, hour=time_now.hour)
    elif by == "day":
        return datetime(time_now.year, time_now.month, day=time_now.day)
    elif by=="month":
        return datetime(time_now.year,time_now.month,day=1)
    elif next_month == True:
        return datetime(time_now.year, time_now.month+1, day=1)
    else:
        return time_now

def validate_datetime_by_fmt(date, date_fmt):
    try:
        datetime.strptime(date,date_fmt)
        return True
    except ValueError:
        return False

if __name__ == '__main__':
    print(validate_datetime_by_fmt("2018-08-18",'%Y-%m-%d'))
    print(validate_datetime_by_fmt("2018-18-08",'%Y-%m-%d'))