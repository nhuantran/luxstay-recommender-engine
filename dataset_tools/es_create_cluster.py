import json
import logging
import os
import sys

sys.path.append(os.getcwd())

from optparse import OptionParser

from utils.load_config import load_config

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO)
LOGGER = logging.getLogger(__name__)

from elasticsearch import Elasticsearch

if __name__ == '__main__':

    parser = OptionParser()

    parser.add_option("-c", "--config_path", dest="config_path", default=None,
                      help="config_path")

    parser.add_option("-m", "--model_path", dest="model_path", default=None,
                      help="Model parsing")

    parser.add_option("-p", "--parser_path", dest="parser_path", default=None,
                      help="Parser path")

    parser.add_option("-i", "--index_name", dest="index_name", default=None,
                      help="Parser path")
    (options, args) = parser.parse_args()

    config_path = options.config_path
    model_path = options.model_path
    parser_path = options.parser_path
    index_name = options.index_name

    config = load_config(config_path)
    if index_name is None:
        if 'PRODUCT_INDEX' in config['ES']:
            index_name = config['ES']['PRODUCT_INDEX']

    clusters = config['ES']['HOSTS']
    clusters_lst = []
    for node in clusters:
        ip, port = node.split(":")
        clusters_lst.append({"host": ip, "port": port})

    es = Elasticsearch(clusters_lst,
                       use_ssl=config['ES']['SSL']['enable'],
                       verify_certs=config['ES']['SSL']['verify_certs'],
                       ca_certs=config['ES']['SSL']['ca_certs'],
                       client_cert=config['ES']['SSL']['client_cert'],
                       client_key=config['ES']['SSL']['client_key'])

    logging.info(es.info())

    model_mapping = json.load(open(model_path))
    analyzer_mapping = json.load(open(parser_path))

    setting = {
        "settings": {
            "analysis": analyzer_mapping
        },
        "mappings": {
            "document": model_mapping,
        },
    }
    logging.info(setting)
    res = es.indices.create(index=index_name, body=setting, ignore=400)
    logging.info(res)
