import os
import sys

sys.path.append(os.getcwd())

from project.models import RoomFeature
from project.controllers.room import RoomFeatureES


def test():
    checkin = "2019-04-14"
    checkout = "2019-04-17"
    room_id = 10067
    number_of_guests = 2
    room_controller = RoomFeatureES()
    current_viewed_room = room_controller.get_by_id(room_id)
    print("query: ",current_viewed_room)
    # .get_nearby_location(current_viewed_room) \
    # .get_similiar_items(current_viewed_room) \
    data = room_controller.get_by_request(checkin, checkout, number_of_guests,current_view_id=current_viewed_room['id'])\
        .get_similar_price(current_viewed_room['price'], threshold=30)\
        .get_nearby_location(current_viewed_room,threshold=10.0)\
        .order_by("last_active","desc") \
        .order_by("city","desc")\
        .order_by("len_description", "desc") \
        .order_by("number_of_photos", "desc") \
        .fetch(6)

    return data
#         .get_similiar_items(current_viewed_room)\


if __name__ == '__main__':
    results= test()
    print("Result")
    for result in results:
        print(result)
