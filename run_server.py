# gunicorn -w 4 -p  main:app
import subprocess
from optparse import OptionParser

if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("-p", "--port", dest="port", type=int,default=8000,
                      help="Port")
    parser.add_option("-b","--host",dest="host",type=str,default="0.0.0.0")
    parser.add_option("-w","--worker",dest="worker",type=int, default=4)

    (options, args) = parser.parse_args()
    port = options.port
    host = options.host
    worker = options.worker

    cmd_run = "gunicorn -w {worker} -b {host} -p {port} main:app".format(worker=worker,host=host,port=port)
    result = subprocess.run([cmd_run],shell=True,check=True)


